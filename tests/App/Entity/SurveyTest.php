<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\SurveyStatus;
use App\Exception\CannotAddAnswerToNotLiveSurveyException;
use App\Exception\InvalidStatusTransitionException;
use PHPUnit\Framework\TestCase;

class SurveyTest extends TestCase
{
    /**
     * @dataProvider provideIncorrectStatusTransitions
     */
    public function testSetStatus_WithIncorrectNewStatus_WillThrowInvalidStatusTransitionException(Survey $survey, SurveyStatus $newStatus): void
    {
        // ASSERT
        $this->expectException(InvalidStatusTransitionException::class);
        $this->expectExceptionMessage(
            sprintf(
                'Status change from "%s" to "%s" is not valid.',
                $survey->getStatus()->value,
                $newStatus->value
            )
        );

        // ACT
        $survey->setStatus($newStatus);
    }

    public static function provideIncorrectStatusTransitions(): array
    {
        $newSurvey = new Survey('name', 'email@test.com');

        return [
            'from new to new' => [
                'survey' => $newSurvey,
                'new status' => SurveyStatus::STATUS_NEW,
            ],
            'from new to closed' => [
                'survey' => $newSurvey,
                'new status' => SurveyStatus::STATUS_CLOSED,
            ],
        ];
    }

    /**
     * @dataProvider provideCorrectStatusTransitions
     */
    public function testSetStatus_WithCorrectNewStatus_WillUpdateStatus(Survey $survey, SurveyStatus $newStatus): void
    {
        // ACT
        $survey->setStatus($newStatus);

        // ASSERT
        $this->assertSame($newStatus, $survey->getStatus());
    }

    public static function provideCorrectStatusTransitions(): array
    {
        $newSurvey = new Survey('name', 'email@test.com');
        $liveSurvey = new Survey('name 2', 'email@test.com');
        $liveSurvey->setStatus(SurveyStatus::STATUS_LIVE);

        return [
            'from new to live' => [
                'survey' => $newSurvey,
                'new status' => SurveyStatus::STATUS_LIVE,
            ],
            'from live to closed' => [
                'survey' => $liveSurvey,
                'new status' => SurveyStatus::STATUS_CLOSED,
            ],
        ];
    }

    public function testAddAnswer_WithNotLiveSurvey_WillThrowCannotAddAnswerToNotLiveSurveyException(): void
    {
        // ARRANGE
        $survey = new Survey('name 2', 'email@test.com');
        $answer = new Answer(2);

        // ASSERT
        $this->expectException(CannotAddAnswerToNotLiveSurveyException::class);
        $this->expectExceptionMessage('Cannot add answer to survey that is not live. Current status: new');

        // ACT
        $survey->addAnswer($answer);
    }
}
