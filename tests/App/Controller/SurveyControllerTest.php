<?php

declare(strict_types=1);

namespace App\Controller;

use App\Cqrs\CommandBus;
use App\Cqrs\QueryBus;
use App\Query\ListSurveysQuery;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class SurveyControllerTest extends TestCase
{
    private MockObject $commandBus;
    private MockObject $queryBus;
    private MockObject $serializer;

    private SurveyController $testSubject;

    public function setUp(): void
    {
        $this->commandBus = $this->createMock(CommandBus::class);
        $this->queryBus = $this->createMock(QueryBus::class);
        $this->serializer = $this->createMock(SerializerInterface::class);

        $this->testSubject = new SurveyController($this->commandBus, $this->queryBus, $this->serializer);
    }

    public function testIndex_WillReturnJsonResponseForCreatedSurvey(): void
    {
        // ARRANGE
        $this->queryBus->method('handle')->willReturn([]);
        $response = 'response-content';
        $this->serializer->method('serialize')->willReturn($response);

        // ACT
        $result = $this->testSubject->index();

        // ASSERT
        $this->assertSame($response, $result->getContent());
        $this->assertSame(Response::HTTP_OK, $result->getStatusCode());
    }

    /**
     * @dataProvider provideIncompleteRequestsForCreate
     */
    public function testCreate_WithMissingNameInRequest_WillThrow(array $requestBody): void
    {
        // ARRANGE
        $this->queryBus->method('handle')->willReturn([]);
        $response = 'response-content';
        $this->serializer->method('serialize')->willReturn($response);

        // ASSERT
        $this->expectException(BadRequestHttpException::class);

        // ACT
        $request = new Request(content: json_encode($requestBody));
        $this->testSubject->create($request);
    }
    public static function provideIncompleteRequestsForCreate(): array
    {
        return [
            'missing name' => [
                [
                    'reportEmail' => 'email@test.com',
                ],
            ],
            'missing reportEmail' => [
                [
                    'name' => 'test',
                ],
            ],
            'empty request' => [
                [
                ],
            ],
        ];
    }
}