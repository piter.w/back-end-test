<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Report;
use App\Entity\Survey;
use App\Repository\ReportRepository;
use App\Repository\SurveyRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;

final class ReportGenerator
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly ReportRepository $reportRepository,
        private readonly SurveyRepository $surveyRepository,
    ) {
    }

    public function generate(string $surveyId): Report
    {
        // getOneWithAnswersEagerLoaded would make it faster because it wouldn't make a query for each answer
        // I'm not sure if that will solve the issue with the memory limit. Probably not and we should
        // create a new repository method that will return a dto containing answers, count and average.
        // Interestingly we don't need to worry about isolation here, because the survey is already closed.
        $survey = $this->surveyRepository->getOneWithAnswersEagerLoaded($surveyId);

        $report = Report::createFromSurvey($survey);

        $this->reportRepository->save($report);

        return $report;
    }
}
