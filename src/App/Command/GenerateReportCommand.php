<?php

declare(strict_types=1);

namespace App\Command;

use App\Cqrs\Command;
use App\Entity\Survey;

class GenerateReportCommand implements Command
{
    public function __construct(public readonly string $surveyId)
    {
    }
}