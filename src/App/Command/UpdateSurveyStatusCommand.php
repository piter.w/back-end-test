<?php

declare(strict_types=1);

namespace App\Command;

use App\Cqrs\Command;
use App\Entity\Survey;
use App\Enum\SurveyStatus;
use Symfony\Component\HttpFoundation\Request;

class UpdateSurveyStatusCommand implements Command
{
    public function __construct(public readonly string $surveyId, public readonly SurveyStatus $status)
    {
    }
}
