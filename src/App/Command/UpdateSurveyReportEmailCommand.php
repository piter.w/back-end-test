<?php

declare(strict_types=1);

namespace App\Command;

use App\Cqrs\Command;
use App\Entity\Survey;
use Symfony\Component\HttpFoundation\Request;

class UpdateSurveyReportEmailCommand implements Command
{
    public function __construct(
        public readonly string $surveyId,
        public readonly string $reportEmail,
    ) {
    }
}
