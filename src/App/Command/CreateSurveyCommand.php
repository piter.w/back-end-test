<?php

declare(strict_types=1);

namespace App\Command;

use App\Cqrs\Command;
use Symfony\Component\HttpFoundation\Request;

class CreateSurveyCommand implements Command
{
    public function __construct(
        public readonly string $name,
        public readonly string $reportEmail,
    ) {
    }
}
