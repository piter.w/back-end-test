<?php

declare(strict_types=1);

namespace App\Command;

use App\Cqrs\Command;
use App\Entity\Survey;
use Symfony\Component\HttpFoundation\Request;

class AddAnswerCommand implements Command
{
    public function __construct(
        public readonly string $surveyId,
        public readonly int $quality,
        public readonly ?string $comment,
    ) {
    }
}
