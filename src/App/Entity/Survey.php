<?php

namespace App\Entity;

use App\Enum\SurveyStatus;
use App\Exception\CannotAddAnswerToNotLiveSurveyException;
use App\Exception\CannotEditSurveyThatIsNotInStatusNewException;
use App\Exception\InvalidStatusTransitionException;
use App\Repository\SurveyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SurveyRepository::class)]
#[UniqueEntity(fields: 'name', errorPath: 'name')]
class Survey
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private UuidInterface $id;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private string $name;

    #[ORM\Column(type: 'string', length: 32, enumType: SurveyStatus::class)]
    private SurveyStatus $status;

    #[ORM\OneToMany(mappedBy: 'survey', targetEntity: Answer::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $answers;

    #[ORM\OneToOne(mappedBy: 'survey', cascade: ['persist', 'remove'])]
    #[Ignore]
    private ?Report $report = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Email]
    private string $reportEmail;

    public function __construct(string $name, string $reportEmail)
    {
        $this->name = $name;
        $this->reportEmail = $reportEmail;

        $this->id = Uuid::uuid4();
        $this->status = SurveyStatus::STATUS_NEW;

        $this->answers = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($this->getStatus() !== SurveyStatus::STATUS_NEW) {
            throw new CannotEditSurveyThatIsNotInStatusNewException();
        }

        $this->name = $name;

        return $this;
    }

    public function getStatus(): SurveyStatus
    {
        return $this->status;
    }

    /**
     * @throws InvalidStatusTransitionException
     */
    public function setStatus(SurveyStatus $newStatus): void
    {
        $this->validateStatusTransition($newStatus);

        $this->status = $newStatus;
    }

    /**
     * @throws InvalidStatusTransitionException
     */
    private function validateStatusTransition(SurveyStatus $newStatus): void
    {
        // new -> live OK
        // live -> closed OK
        if ($this->status === SurveyStatus::STATUS_NEW && $newStatus === SurveyStatus::STATUS_LIVE) {
            return;
        }

        if ($this->status === SurveyStatus::STATUS_LIVE && $newStatus === SurveyStatus::STATUS_CLOSED) {
            return;
        }

        throw new InvalidStatusTransitionException(
            sprintf(
                'Status change from "%s" to "%s" is not valid.',
                $this->status->value,
                $newStatus->value
            )
        );
    }

    /**
     * @return Collection<int, Answer>
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer): self
    {
        if ($this->getStatus() !== SurveyStatus::STATUS_LIVE) {
            throw new CannotAddAnswerToNotLiveSurveyException(
                sprintf('Cannot add answer to survey that is not live. Current status: %s', $this->getStatus()->value)
            );
        }

        if (!$this->answers->contains($answer)) {
            $this->answers->add($answer);
            $answer->setSurvey($this);
        }

        return $this;
    }

    public function getReport(): ?Report
    {
        return $this->report;
    }

    public function getReportEmail(): string
    {
        return $this->reportEmail;
    }

    public function setReportEmail(string $reportEmail): self
    {
        if ($this->getStatus() !== SurveyStatus::STATUS_NEW) {
            throw new CannotEditSurveyThatIsNotInStatusNewException();
        }

        $this->reportEmail = $reportEmail;

        return $this;
    }
}
