<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: ReportRepository::class)]
class Report
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private ?UuidInterface $id = null;

    #[ORM\Column]
    private ?int $numberOfAnswers = null;

    #[ORM\Column]
    private ?int $quality = null;

    #[ORM\Column(type: Types::JSON)]
    private array $comments = [];

    #[ORM\Column]
    private ?\DateTimeImmutable $generatedAt = null;

    #[ORM\OneToOne(inversedBy: 'report', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(unique: true, nullable: false)]
    #[Ignore]
    private ?Survey $survey = null;

    private function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->generatedAt = new \DateTimeImmutable();
    }

    public static function createFromSurvey(Survey $survey): self
    {
        $report = new Report();
        $sum = 0;
        $numberOfAnswers = $survey->getAnswers()->count();

        if ($numberOfAnswers === 0) {
            return $report;
        }

        $comments = [];
        foreach ($survey->getAnswers() as $answer) {
            $sum += (int) $answer->getQuality();
            if ($answer->getComment() !== null) {
                $comments[] = $answer->getComment();
            }
        }
        $quality = (int) ($sum / $numberOfAnswers);

        $report->survey = $survey;
        $report->quality = $quality;
        $report->numberOfAnswers = $numberOfAnswers;
        $report->comments = $comments;

        return $report;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getNumberOfAnswers(): ?int
    {
        return $this->numberOfAnswers;
    }

    public function getQuality(): ?int
    {
        return $this->quality;
    }

    public function getComments(): array
    {
        return $this->comments;
    }

    public function getGeneratedAt(): ?\DateTimeImmutable
    {
        return $this->generatedAt;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }
}
