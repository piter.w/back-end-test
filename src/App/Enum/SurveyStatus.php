<?php

declare(strict_types=1);

namespace App\Enum;

/**
 * Introducing this enum to improve type safety.
 */
enum SurveyStatus: string
{
    case STATUS_NEW = 'new';
    case STATUS_LIVE = 'live';
    case STATUS_CLOSED = 'closed';
}
