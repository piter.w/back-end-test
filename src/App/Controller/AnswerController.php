<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\AddAnswerCommand;
use App\Cqrs\CommandBus;
use App\Entity\Survey;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;

final class AnswerController
{
    public function __construct(
        private readonly CommandBus $commandBus,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/surveys/{id}/answers', methods: 'POST')]
    public function create(Survey $survey, Request $request): JsonResponse
    {
        $requestBody = json_decode($request->getContent(), true);
        if (empty($requestBody['quality'])) {
            throw new BadRequestHttpException();
        }

        try {
            $quality = $requestBody['quality'];
            $comment = $requestBody['comment'] ?? null;

            $survey = $this->commandBus->dispatch(new AddAnswerCommand((string) $survey->getId(), $quality, $comment));

            $surveyData = $this->serializer->serialize($survey, 'json');

            return new JsonResponse($surveyData, Response::HTTP_CREATED, json: true);
        } catch (HandlerFailedException $e) {
            if (!empty($e->getNestedExceptionOfClass(AccessDeniedException::class))) {
                throw new AccessDeniedHttpException(previous: $e);
            }
            throw new BadRequestHttpException();
        }
    }
}
