<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\CreateSurveyCommand;
use App\Command\DeleteSurveyCommand;
use App\Command\UpdateSurveyNameCommand;
use App\Command\UpdateSurveyReportEmailCommand;
use App\Command\UpdateSurveyStatusCommand;
use App\Cqrs\CommandBus;
use App\Cqrs\QueryBus;
use App\Entity\Survey;
use App\Enum\SurveyStatus;
use App\Query\ListSurveysQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;
use ValueError;

#[Route('/surveys')]
class SurveyController
{
    public function __construct(
        private readonly CommandBus $commandBus,
        private readonly QueryBus $queryBus,
        private readonly SerializerInterface $serializer,
    ) {
    }

    #[Route(methods: 'GET')]
    public function index(): JsonResponse
    {
        $result = $this->queryBus->handle(new ListSurveysQuery());

        return new JsonResponse($this->serializer->serialize($result, 'json'), json: true);
    }

    #[Route(methods: 'POST')]
    public function create(Request $request): JsonResponse
    {
        $requestBody = json_decode($request->getContent(), true);
        if (empty($requestBody['name']) || empty($requestBody['reportEmail'])) {
            throw new BadRequestHttpException();
        }

        try {
            $command = new CreateSurveyCommand($requestBody['name'], $requestBody['reportEmail']);
            $survey = $this->commandBus->dispatch($command);

            $surveyData = $this->serializer->serialize($survey, 'json');

            return new JsonResponse($surveyData, Response::HTTP_CREATED, json: true);
        } catch (HandlerFailedException $e) {
            throw new UnprocessableEntityHttpException();
        }
    }

    #[Route('/{id}', methods: 'PUT')]
    public function edit(Survey $survey, Request $request): JsonResponse
    {
        $requestBody = json_decode($request->getContent(), true);

        try {
            if (!empty($requestBody['name'])) {
                $command = new UpdateSurveyNameCommand((string) $survey->getId(), $requestBody['name']);
                $survey = $this->commandBus->dispatch($command);
            }
            if (!empty($requestBody['reportEmail'])) {
                $command = new UpdateSurveyReportEmailCommand((string) $survey->getId(), $requestBody['reportEmail']);
                $survey = $this->commandBus->dispatch($command);
            }

            $surveyData = $this->serializer->serialize($survey, 'json');

            return new JsonResponse($surveyData, Response::HTTP_CREATED, json: true);
        } catch (HandlerFailedException) {
            throw new UnprocessableEntityHttpException();
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Survey $survey): JsonResponse
    {
        try {
            $command = new DeleteSurveyCommand((string) $survey->getId());
            $this->commandBus->dispatch($command);

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        } catch (HandlerFailedException $e) {
            if (!empty($e->getNestedExceptionOfClass(AccessDeniedException::class))) {
                throw new AccessDeniedHttpException(previous: $e);
            }
            throw new UnprocessableEntityHttpException(previous: $e);
        }
    }

    #[Route('/{id}/status', methods: 'PATCH')]
    public function changeStatus(Survey $survey, Request $request): JsonResponse
    {
        $requestBody = json_decode($request->getContent(), true);

        if (empty($requestBody['status'])) {
            throw new BadRequestHttpException();
        }

        try {
            $status = SurveyStatus::from($requestBody['status']);
            $command = new UpdateSurveyStatusCommand((string) $survey->getId(), $status);
            $this->commandBus->dispatch($command);
            $surveyData = $this->serializer->serialize($survey, 'json');

            $statusCode = $survey->getStatus() === SurveyStatus::STATUS_CLOSED
                ? Response::HTTP_ACCEPTED
                : Response::HTTP_OK;

            return new JsonResponse($surveyData, $statusCode, json: true);
        } catch (HandlerFailedException $e) {
            if (!empty($e->getNestedExceptionOfClass(AccessDeniedException::class))) {
                throw new AccessDeniedHttpException(previous: $e);
            }
            throw new UnprocessableEntityHttpException(previous: $e);
        } catch (ValueError) {
            throw new UnprocessableEntityHttpException();
        }
    }
}
