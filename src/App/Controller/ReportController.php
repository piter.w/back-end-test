<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Report;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ReportController
{
    public function __construct(
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/reports/{id}', methods: 'GET')]
    public function show(Report $report): JsonResponse
    {
        $reportData = $this->serializer->serialize($report, 'json');

        return new JsonResponse($reportData, Response::HTTP_OK, json: true);
    }
}
