<?php

declare(strict_types=1);

namespace App\QueryHandler;

use App\Cqrs\QueryHandler;
use App\Entity\Survey;
use App\Query\ListSurveysQuery;
use App\Repository\SurveyRepository;

class ListSurveysQueryHandler implements QueryHandler
{
    public function __construct(private readonly SurveyRepository $surveyRepository)
    {
    }

    /**
     * @return Survey[]
     */
    public function __invoke(ListSurveysQuery $query): array
    {
        return $this->surveyRepository->findAll();
    }
}
