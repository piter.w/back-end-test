<?php

namespace App\Repository;

use App\Entity\Survey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Survey>
 *
 * @method Survey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Survey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Survey[]    findAll()
 * @method Survey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Survey::class);
    }

    public function save(Survey $entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    public function remove(Survey $entity): void
    {
        $this->getEntityManager()->remove($entity);
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getOneWithAnswersEagerLoaded(string $surveyId): Survey
    {
        $queryBuilder = $this->createQueryBuilder('s');
        $queryBuilder->select('s, a');
        $queryBuilder->join('s.answers', 'a');
        $queryBuilder->where('s.id = :surveyId');
        $queryBuilder->setParameter('surveyId', $surveyId);

        return $queryBuilder->getQuery()->getSingleResult();
    }
}
