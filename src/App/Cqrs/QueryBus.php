<?php

declare(strict_types=1);

namespace App\Cqrs;

interface QueryBus
{
    public function handle(Query $query);
}