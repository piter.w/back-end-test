<?php

declare(strict_types=1);

namespace App\Cqrs;

interface CommandBus
{
    public function dispatch(Command $command): mixed;
}