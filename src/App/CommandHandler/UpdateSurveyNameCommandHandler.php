<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\UpdateSurveyReportEmailCommand;
use App\Cqrs\CommandHandler;
use App\Entity\Survey;
use App\Repository\SurveyRepository;
use App\Security\Voter\SurveyVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateSurveyNameCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly ValidatorInterface $validator,
        private readonly AuthorizationCheckerInterface $authorizationChecker,
    ) {
    }

    public function __invoke(UpdateSurveyReportEmailCommand $command): Survey
    {
        $survey = $this->surveyRepository->find($command->surveyId);
        if (!$this->authorizationChecker->isGranted(SurveyVoter::EDIT, $survey)) {
            throw new AccessDeniedException();
        }
        $survey->setReportEmail($command->reportEmail);

        $errors = $this->validator->validate($survey);

        if ($errors->count() > 0) {
            throw new ValidatorException();
        }

        return $survey;
    }
}
