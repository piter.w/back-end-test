<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\CreateSurveyCommand;
use App\Cqrs\CommandHandler;
use App\Entity\Survey;
use App\Repository\SurveyRepository;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateSurveyCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly ValidatorInterface $validator,
    ) {
    }

    public function __invoke(CreateSurveyCommand $command): Survey
    {
        $survey = new Survey($command->name, $command->reportEmail);

        $errors = $this->validator->validate($survey);
        if ($errors->count() > 0) {
            throw new ValidatorException();
        }

        $this->surveyRepository->save($survey);

        return $survey;
    }
}
