<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\GenerateReportCommand;
use App\Cqrs\CommandHandler;
use App\Service\ReportGenerator;
use App\Service\ReportMailer;

class GenerateReportCommandHandler implements CommandHandler
{
    public function __construct(private readonly ReportGenerator $reportGenerator, private readonly ReportMailer $reportMailer,)
    {
    }

    public function __invoke(GenerateReportCommand $command): void
    {
        $report = $this->reportGenerator->generate($command->surveyId);

        $this->reportMailer->send($report);
    }
}
