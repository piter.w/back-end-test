<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\GenerateReportCommand;
use App\Command\UpdateSurveyStatusCommand;
use App\Cqrs\CommandBus;
use App\Cqrs\CommandHandler;
use App\Entity\Survey;
use App\Enum\SurveyStatus;
use App\Repository\SurveyRepository;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateSurveyStatusCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly ValidatorInterface $validator,
        private readonly CommandBus $commandBus,
    ) {
    }

    public function __invoke(UpdateSurveyStatusCommand $command): Survey
    {
        $survey = $this->surveyRepository->find($command->surveyId);
        $survey->setStatus($command->status);

        $errors = $this->validator->validate($survey);
        if ($errors->count() > 0) {
            throw new ValidatorException();
        }

        try {
            if ($survey->getStatus() === SurveyStatus::STATUS_CLOSED) {
                $this->commandBus->dispatch(new GenerateReportCommand((string) $survey->getId()));
            }
        } catch (\Throwable $e) {
            dd($e);
        }


        return $survey;
    }
}
