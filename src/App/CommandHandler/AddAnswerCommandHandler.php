<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\AddAnswerCommand;
use App\Cqrs\CommandHandler;
use App\Entity\Answer;
use App\Entity\Survey;
use App\Repository\SurveyRepository;
use App\Security\Voter\SurveyVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AddAnswerCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly AuthorizationCheckerInterface $authorizationChecker,
        private readonly SurveyRepository $surveyRepository,
        private readonly ValidatorInterface $validator,
    ) {
    }

    public function __invoke(AddAnswerCommand $command): Survey
    {
        $survey = $this->surveyRepository->find($command->surveyId);
        if (!$this->authorizationChecker->isGranted(SurveyVoter::ANSWER, $survey)) {
            throw new AccessDeniedException();
        }

        $answer = new Answer($command->quality, $command->comment);
        $survey->addAnswer($answer);

        $errors = $this->validator->validate($answer);
        if ($errors->count() > 0) {
            throw new ValidatorException();
        }

        return $survey;
    }
}
