<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\DeleteSurveyCommand;
use App\Cqrs\CommandHandler;
use App\Repository\SurveyRepository;
use App\Security\Voter\SurveyVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DeleteSurveyCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly AuthorizationCheckerInterface $authorizationChecker,
    ) {
    }

    public function __invoke(DeleteSurveyCommand $command): void
    {
        $survey = $this->surveyRepository->find($command->surveyId);
        if (!$this->authorizationChecker->isGranted(SurveyVoter::DELETE, $survey)) {
            throw new AccessDeniedException();
        }

        $this->surveyRepository->remove($survey);
    }
}
